" Plug
call plug#begin('~/.local/share/nvim/plugged')

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

Plug 'liuchengxu/vista.vim' " LSP (coc) tags bar

Plug 'dracula/vim', { 'as': 'dracula' }

Plug 'tpope/vim-surround'
Plug 'scrooloose/syntastic'

" deoplete.nvim for autocompletion
"     NOTE: DEOPLETE COMMENTED FOR COC-NVIM
"     if has('nvim')
"         Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
"     else
"         Plug 'Shougo/deoplete.nvim'
"         Plug 'roxma/nvim-yarp'
"         Plug 'roxma/vim-hug-neovim-rpc'
"     endif
"     let g:deoplete#enable_at_startup = 1

" Conquer of Completion (CoC)
Plug 'neoclide/coc.nvim', {'branch': 'release'}

Plug 'petRUShka/vim-opencl'

" Plug 'airblade/vim-gitgutter'

" Should provide typescript linting?
Plug 'neomake/neomake'
Plug 'HerringtonDarkholme/yats.vim'
Plug 'mhartington/nvim-typescript', { 'do': './install.sh' }
Plug 'leafgarland/typescript-vim'

Plug 'arrufat/vala.vim'

call plug#end()

" Airline (powerline)
let g:airline_powerline_fonts = 1
let g:airline_theme='powerlineish'

nmap ge :CocCommand explorer<CR>

inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

colorscheme dracula

" disables automatic code folding for vala (and other languages as well probably)
set nofoldenable

set mouse=a
set number

set guifont=Fira\ Mono:h14

filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab

set background=dark

set termguicolors

set colorcolumn=80
tnoremap <Esc> <C-\><C-n>
