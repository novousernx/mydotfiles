# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
export TERM="xterm-256color"
#POWERLEVEL9K_MODE='awesome-fontconfig'
POWERLEVEL9K_MODE='nerdfont-complete'

ZSH_THEME="powerlevel10k/powerlevel10k"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins+=(git flatpak)

# User configuration

export PATH="$PATH:/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/lib/jvm/default/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/home/gabmus/.local/bin"
# export MANPATH="/usr/local/man:$MANPATH"

source $ZSH/oh-my-zsh.sh

# You may need to manually set your language environment
# export LANG=en_US.UTF-8-1

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
. /etc/profile.d/vte.sh


#Powerlevel9k config vvv

# POWERLEVEL9K_SHORTEN_DIR_LENGTH=2
POWERLEVEL9K_PROMPT_ON_NEWLINE=true
#POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX="↱"
#POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX="↳ "

# POWERLEVEL9K_VI_MODE_INSERT_FOREGROUND='black'
# POWERLEVEL9K_VI_MODE_INSERT_BACKGROUND='green'
# POWERLEVEL9K_VI_MODE_NORMAL_FOREGROUND='white'
# POWERLEVEL9K_VI_MODE_NORMAL_BACKGROUND='blue'

POWERLEVEL9K_VCS_GIT_ICON=''
POWERLEVEL9K_VCS_STAGED_ICON='\u00b1'
POWERLEVEL9K_VCS_UNTRACKED_ICON='\u25CF'
POWERLEVEL9K_VCS_UNSTAGED_ICON='\u00b1'
POWERLEVEL9K_VCS_INCOMING_CHANGES_ICON='\u2193'
POWERLEVEL9K_VCS_OUTGOING_CHANGES_ICON='\u2191'

POWERLEVEL9K_LEFT_SUBSEGMENT_SEPARATOR="\b"
POWERLEVEL9K_CUSTOM_ARCH="echo '\uf303'"
POWERLEVEL9K_CUSTOM_ARCH_BACKGROUND="cyan"
POWERLEVEL9K_CUSTOM_ARCH_FOREGROUND="black"

POWERLEVEL9K_CONTEXT_DEFAULT_BACKGROUND="black"
POWERLEVEL9K_CONTEXT_DEFAULT_FOREGROUND="yellow"

POWERLEVEL9K_MULTILINE_FIRST_PROMPT_PREFIX=""
#POWERLEVEL9K_MULTILINE_SECOND_PROMPT_PREFIX="%K{white}%F{black} `date +%T` \UE12E %f%k%F{white}%f "
POWERLEVEL9K_MULTILINE_LAST_PROMPT_PREFIX="%K{blue} \uf155 %F{blue}%k\ue0b0%f "

POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(custom_arch context dir vcs background_jobs status nvm rvm) # vi_mode)
POWERLEVEL9K_RIGHT_PROMPT_ELEMENTS=()

POWERLEVEL9K_DIR_HOME_BACKGROUND="green"
POWERLEVEL9K_VIRTUALENV_BACKGROUND="green"
POWERLEVEL9K_OS_ICON_BACKGROUND="green"
POWERLEVEL9K_OS_ICON_FOREGROUND="black"
POWERLEVEL9K_BACKGROUND_JOBS_BACKGROUND="red"

export EDITOR=nvim

PATH=$PATH:$HOME/.bin

alias ip="ip -c"
alias pacman="pacman --color=always"

if [ -f "$(which rg)" ]; then
    alias grep="rg"
    alias grepi="rg -i"
else
    alias grepi="grep -i"
fi

alias wifistatus="iw dev wlp5s0 info"

alias restartbtdrv="sudo sh -c 'rmmod btusb && modprobe btusb'"
alias rstnm="sudo systemctl restart NetworkManager"
alias whoowns="pacman -Qo"
alias governor_performance="sudo sh -c 'echo performance | tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor'"
alias governor_powersave="sudo sh -c 'echo powersave | tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor'"

[ -f "$(which nvim)" ] && alias vim="nvim"
if [ -f "$(which nvimpager)" ]; then
    export PAGER=nvimpager
elif [ -f "$(which most)" ]; then
    export PAGER=most
fi
[ -f "$(which lsd)" ] && alias ls="lsd --group-dirs first"

if [ $USERNAME = 'gabmus' ]; then
    export LANG="en_US.UTF-8"
    export LC_ALL="en_US.UTF-8"
    export LC_COLLATE="en_US.UTF-8"
    export LC_CTYPE="en_US.UTF-8"
    export LC_MESSAGES="en_US.UTF-8"
    export LC_MONETARY="en_UK.UTF-8"
    export LC_NUMERIC="en_UK.UTF-8"
    export LC_TIME="en_UK.UTF-8"
    # bindkey -v # use VI mode
fi

# disable winemenubuilder (will not create desktop files)
export WINEDLLOVERRIDES=winemenubuilder.exe=d
export GOPATH=$HOME/Apps/gopath
export GOBIN=$GOPATH/bin
export PATH=$PATH:$GOBIN:$HOME/.cargo/bin:$HOME/.emacs.d/bin
alias drop_caches="sudo sh -c \"echo 3 > /proc/sys/vm/drop_caches\""
